// import logo from './logo.svg';
import './App.css';
import {useState} from 'react'
// import Data from './data';
import { Users } from './json';

function App() {
  const [search, setSearch] = useState("");
  return (
    <div className="app">
      <input className="search" placeholder="Search here" onChange={(e) => setSearch(e.target.value.toLowerCase())}/>
      <ul className="list">
        {Users.filter((srch) =>
          srch.first_name.toLowerCase().includes(search)
        ).map((user) => (
          <li className="listItem" key={user.id}>
            {user.first_name}
          </li>
          
        ))}
      </ul>
    </div>
  
  );
}
export default App
