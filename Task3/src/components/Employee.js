import React, { Component } from "react";
import PropTypes from "prop-types";

class Employee extends Component {
    render() {
        return (
            <div>
                <h1> Hello, {this.props.name}</h1>
                <h3> Your EmpId -- {this.props.empId} </h3>
                <h3> Your Skill -- {this.props.arr}</h3>
                <h3> Your Designation -- {this.props.object.X}</h3>
                <h3> Employee Info -- { this.props.fun()} </h3>
            </div>
        )
    }
}

Employee.propTypes = {
    name: PropTypes.string.isRequired,
    empId: PropTypes.number.isRequired,
    arr: PropTypes.array.isRequired,
    object:PropTypes.object.isRequired,
    fun: PropTypes.func.isRequired

};


Employee.defaultProps = {
    name: "Priyanka",
    empId: "1001",
    arr: ['C,', 'C++,', 'PHP,', 'Java'],
    object: "Developer"
}
export default Employee;