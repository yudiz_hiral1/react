import React, { Component } from 'react'
// import './reg.css'

class Register extends Component {
  constructor () {
    super()
    this.state = {
      fullname: '',
      email: '',
      password: '',
      city: '',
      mobileno: '',
      dob: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.submitHandler = this.submitHandler.bind(this)
  }

  handleChange = (event) => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({ [name]: value })
  }

  submitHandler = (event) => {
    console.log(this.state)
    event.preventDefault()
  }

  render () {
    return (
      <div className='container'>
        <div className="title">Registration</div>
        <br />
        <form id="login_form" method="" action="#" name="regi_form" onSubmit={this.submitHandler}>
          <div className="user-details">

            <div className="input-box">
              <span className="details">Name</span>
              <input type="text" id="fullname" name="fullname" placeholder="Enter Name" value={this.state.fullname} onChange={this.handleChange} required />
            </div>

            <div className="input-box">
              <span className="details">Email</span>
              <input type="text" id="email" name="email" placeholder="Enter Email" value={this.state.email} onChange={this.handleChange} required />
            </div>
            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" id="pwd" name="password" placeholder="Enter password" value={this.state.password} onChange={this.handleChange} required />
            </div>

            <div className="input-box">
              <span className="details">City</span>
              <input type="text" id="city" name="city" placeholder="Enter your city"value={this.state.city} onChange={this.handleChange} required />
            </div>

            <div className="input-box">
              <span className="details">Mobile Number</span>
              <input type="tel" id="mobileno" name="mobileno" placeholder="Enter Mobile Number" value={this.state.mobileno} onChange={this.handleChange} required />
            </div>

            <div className="input-box">
              <span className="details">Date of Birth</span>
              <input type="date" id="birtdate" name="dob" placeholder="Enter Date of Birth" value={this.state.dob} onChange={this.handleChange} required />
            </div>

            {/* <div className="gender">
              <span className="details">Gender</span> <br />
              <div className="input-box-1">
                <label htmlFor="male">Male</label>
                <input type="radio" name="gender" id="male" value="male" />

                <label htmlFor="female">Female</label>
                <input type="radio" name="gender" id="female" value="female" />
              </div>
            </div> */}

          </div>
          <div className="button">
            <input type="submit" value="Register" className="btn" />
          </div>

        </form>
      </div>
    )
  }
}

export default Register
