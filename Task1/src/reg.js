import React, {useState}from 'react'
// import './Registration.css'
import './reg.css'

export default function Registration () {

  const [fullname, setFullname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [city, setCity] = useState('')
  const [mobileno, setMobileno] = useState('')
  const [dob, setDob] = useState('')
  // const [gender, setGender] = useState('')


  const handleChange = (e) => {
    const {id, value} = e.target

    if( id === 'fullname') {
      setFullname(value)
    }

    if( id === 'email') {
      setEmail(value)
    }

    if( id === 'pwd') {
      setPassword(value)
    }

    if( id === 'city') {
      setCity(value)
    }

    if( id === 'mobileno') {
      setMobileno(value)
    }

    if( id === 'birtdate') {
      setDob(value)
    }
  }  


    const submit = (e) => {
      console.log (fullname, email, password, city, mobileno, dob,)
      e.preventDefault()
    }

  return (
      <div className='container'>
        <div className="title">Registration</div>
        <br />
        <form id="login_form" method="" action="#" name="regi_form" onSubmit={submit}>
          <div className="user-details">

            <div className="input-box">
              <span className="details">Name</span>
              <input type="text" id="fullname" name="fullname" placeholder="Enter Name" value={fullname} onChange= {(e) => handleChange (e)} required/>
            </div>

            <div className="input-box">
              <span className="details">Email</span>
              <input type="text" id="email" name="email" placeholder="Enter Email" value={email} onChange= {(e) => handleChange (e)} required/>
            </div>

            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" id="pwd" name="password" placeholder="Enter password" value={password} onChange= {(e) => handleChange (e)} required/>
            </div>

            <div className="input-box">
              <span className="details">City</span>
              <input type="text" id="city" name="add" placeholder="Enter your city" value={city} onChange= {(e) => handleChange (e)} required/>
            </div>

            <div className="input-box">
              <span className="details">Mobile Number</span>
              <input type="tel" id="mobileno" name="phone" placeholder="Enter Mobile Number" value={mobileno} onChange= {(e) => handleChange (e)} required/>
            </div>

            <div className="input-box">
              <span className="details">Date of Birth</span>
              <input type="date" id="birtdate" name="dob" placeholder="Enter Date of Birth" value={dob} onChange= {(e) => handleChange (e)} required/>
            </div>

            {/* <div className="gender">
              <span className="details">Gender</span> <br />
              <div className="input-box-1">
                <label htmlFor="male">Male</label>
                <input type="radio" name="gender" id="male" value="male" />

                <label htmlFor="female">Female</label>
                <input type="radio" name="gender" id="female" value="female" />
              </div>
            </div> */}

          </div>
          <div className="button">
            <input type="submit" value="Register" className="btn" />
          </div>

        </form>
      </div>
  )
}
